package Trees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import Trees.TreeNode;

/**
 * PostOrder
 */
public class PostOrder {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1); 
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    // postOrderTraversal(root);
    List<Integer> res = postOrderUsingStack(root);
    System.out.println(res);
  }

  static void postOrderTraversal(TreeNode root) {
    if(root == null ) return;

    postOrderTraversal(root.left);
    postOrderTraversal(root.right);
    System.out.print(root.val + " -> ");
  }

  static List<Integer> postOrderUsingStack(TreeNode root) {
    TreeNode curr = root;
    Stack<TreeNode> stack = new Stack<>();
    List<Integer> res = new ArrayList<>();
    stack.push(curr);

    while(!stack.isEmpty()) {
      curr = stack.peek();
      stack.pop();
      res.add(curr.val);

      if(curr.left != null) stack.push(curr.left);
      if(curr.right != null) stack.push(curr.right);
    }
    Collections.reverse(res);
    return res;
  }
}
