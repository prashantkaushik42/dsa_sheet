package Trees;

import java.util.ArrayList;

import Trees.TreeNode;

/**
 * PathToRoot
 */
public class PathToRoot {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1); 
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    ArrayList<Integer> res = path(root, 7);
    System.out.println(res);
  }

  static ArrayList<Integer> path(TreeNode node, int x) {
    ArrayList<Integer> res = new ArrayList<>();
    helper(node, x, res);
    return res;
  }

  static boolean helper(TreeNode node, int x, ArrayList<Integer> res) {
    if(node == null) return false;

    res.add(node.val);
    if(node.val == x) {
      return true;
    }

    if(helper(node.left, x, res) || helper(node.right, x, res)) return true;
    res.remove(res.size()-1);
    return false;
  }
}
