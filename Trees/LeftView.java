package Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import Trees.TreeNode;

/**
 * LeftView
 */
public class LeftView {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    ArrayList<Integer> res = leftView(root);
    System.out.println(res);
  }

  static ArrayList<Integer> leftView(TreeNode root) {
    Queue<TreeNode> queue = new LinkedList<>();
    ArrayList<Integer> res = new ArrayList<>();

    if (root == null)
      return res;
    queue.add(root);
    TreeNode node;
    int takesFirst = 1;
    while (!queue.isEmpty()) {
      int qLen = queue.size();
      takesFirst = 1;
      for (int i = 1; i <= qLen; i++) {
        node = queue.poll();
        if (takesFirst-- > 0)
          res.add(node.val);
        if (node.left != null)
          queue.add(node.left);
        if (node.right != null)
          queue.add(node.right);
      }
    }
    return res;
  }

}
