package Trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * InOrder
 */
public class InOrder {
  static class Node{
    int data;
    Node left;
    Node right;

    Node(int data) {
      this.data = data;
      left = null;
      right = null;
    }
  }

  public static void main(String[] args) {
    Node root = new Node(1); 
    root.left = new Node(2);
    root.left.left = new Node(4);
    root.left.right = new Node(5);
    root.right = new Node(3);
    root.right.right = new Node(7);
    root.right.left = new Node(6);
    root.right.left.right = new Node(8);

    // inorderTraversal(root);
    List<Integer> res = inOrderUsingStack(root);
    System.out.println(res);
  }

  static void inorderTraversal(Node root) {
    if(root == null) return;

    inorderTraversal(root.left);
    System.out.print(root.data + " -> ");
    inorderTraversal(root.right);
  }

  static List<Integer> inOrderUsingStack(Node root) {
    Node curr = root;
    List<Integer> res = new ArrayList<>();
    Stack<Node> stack = new Stack<>();

    while(curr != null || !stack.isEmpty()) {
      while(curr != null) {
        stack.push(curr);
        curr = curr.left;
      }

      curr = stack.pop();
      res.add(curr.data);
      curr = curr.right;
    }
    return res;
  }
}
