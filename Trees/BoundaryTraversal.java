package Trees;

import java.util.ArrayList;
import java.util.Collections;

import Trees.TreeNode;

/**
 * BoundaryTraversal
 */
public class BoundaryTraversal {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1); 
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    ArrayList<Integer> res = boundary(root);
    System.out.println(res);
  }

  static ArrayList<Integer> boundary(TreeNode node) {
    ArrayList<Integer> res = new ArrayList<>();
    // this is for the root element
    if(!isLeaf(node)) res.add(node.data);
    addLeftBoundary(node, res);
    addLeaves(node, res);
    addRightBoundary(node, res);
    return res;
  }

  static boolean isLeaf(TreeNode node){
    return node.left == null && node.right == null;
  }

  static void addLeftBoundary(TreeNode node, ArrayList<Integer> res) {
    TreeNode curr = node.left;
    while(curr != null) {
      if(!isLeaf(curr)) res.add(curr.data);
      if(curr.left != null) curr = curr.left;
      else curr = curr.right;
    }
  }

  static void addLeaves(TreeNode node, ArrayList<Integer> res) {
    if(isLeaf(node)) {
      res.add(node.data);
      return;
    }

    if(node.left != null)  addLeaves(node.left, res);
    if(node.right != null)  addLeaves(node.right, res);
  }

  static void addRightBoundary(TreeNode node, ArrayList<Integer> res) {
    TreeNode curr = node.right;
    ArrayList<Integer> temp = new ArrayList<>();
    while(curr != null) {
      if(!isLeaf(curr)) temp.add(curr.data);
      if(curr.right != null) curr = curr.right;
      else curr = curr.left;
    }

    // reverse the list because it will gets the right in reverse
    Collections.reverse(temp);
    res.addAll(temp);
  }
}
