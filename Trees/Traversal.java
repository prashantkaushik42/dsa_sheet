package Trees;

import java.util.ArrayList;
import java.util.List;

import Trees.TreeNode;

/**
 * Traversal
 */
public class Traversal {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    List<List<Integer>> res = allTraversals(root);
    System.out.println(res);
  }

  static List<List<Integer>> allTraversals(TreeNode root) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> preorder = new ArrayList<>();
    List<Integer> postorder = new ArrayList<>();
    List<Integer> inorder = new ArrayList<>();
    helper(root, preorder, postorder, inorder);
    res.add(preorder);
    res.add(postorder);
    res.add(inorder);
    return res;
  }

  static void helper(TreeNode root, List<Integer> preorder, List<Integer> postorder,
      List<Integer> inorder) {
    if(root == null) return;

    preorder.add(root.val);
    helper(root.left, preorder, postorder, inorder);
    inorder.add(root.val);
    helper(root.right, preorder, postorder, inorder);
    postorder.add(root.val);
  }
}
