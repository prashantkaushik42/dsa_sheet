package Trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;

import Trees.TreeNode;

public class MaxWidth {
  static class Pair {
    int hd;
    TreeNode node;

    Pair(int hd, TreeNode node) {
      this.hd = hd;
      this.node = node;
    }
  }

  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    int width = maxWidth(root);
    System.out.println(width);
  }

    static int maxWidth(TreeNode root) {
      LinkedList<pair> q = new LinkedList<>();
      int max = Integer.MIN_VALUE;
      if (root == null)
        return 0; // why not null coz see eg 1 when 2 L will come with null we cant add anything
                  // to it
      q.addLast(new pair(root, 0));

      while (!q.isEmpty()) {
        int size = q.size();

        max = Math.max(max, (q.getLast().level - q.getFirst().level + 1));

        for (int i = 0; i < size; i++) {
          pair ele = q.removeFirst();
          if (ele.root.left != null) {
            q.addLast(new pair(ele.root.left, 2 * ele.level));
          }
          if (ele.root.right != null) {
            q.addLast(new pair(ele.root.right, 2 * ele.level + 1));
          }
        }
      }
      return max;
    }

    // making a custom pair class to get the root and level
    static class pair {
      int level;
      TreeNode root;

      public pair(TreeNode root, int level) {
        this.level = level;
        this.root = root;
      }
    }
  }
