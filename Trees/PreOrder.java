package Trees;
import java.util.*;

/**
 * PreOrder
 */
public class PreOrder {
  static class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int val) {
      this.val = val;
      left = null;
      right = null;
    }
  }

  public static void main(String[] args) {
    TreeNode root = new TreeNode(1); 
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);
    preOrderTraversal(root);
    List<Intger> res =preOrderUsingStack(root);
  }

  static void preOrderTraversal(TreeNode root) {
    if(root == null) return;

    System.out.print(root.val + " -> ");
    preOrderTraversal(root.left);
    preOrderTraversal(root.right);
  }

  static List<Integer> preOrderUsingStack(TreeNode root) {
    TreeNode curr = root;
    Stack<TreeNode> stack = new Stack<>();
    stack.push(curr);

    while(curr != null && !stack.isEmpty()){
      curr = stack.peek();
      list.add(curr.val);
      stack.pop();
      
      // we firstly add right because we need left as our peek
      if(curr.right != null) stack.push(curr.right);
      if(curr.left != null) stack.push(curr.left);
    }
  }
}
