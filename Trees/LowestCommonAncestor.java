package Trees;

import Trees.TreeNode;

/**
 * LowestCommonAncestor
 */
public class LowestCommonAncestor {
  TreeNode ans = null;

  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    TreeNode node = commonAncestor(root);
    System.out.println(node);
  }

  public TreeNode commonAncestor(TreeNode root, TreeNode p, TreeNode q) {
    recurseTree(root, p, q);
    return this.ans;
  }

  public boolean recurseTree(TreeNode root, TreeNode p, TreeNode q) {
    if (root == null)
      return false;

    int left = recurseTree(root.left, p, q) ? 1 : 0;
    int right = recurseTree(root.right, p, q) ? 1 : 0;
    int mid = (root == p || root == q) ? 1 : 0;

    if (mid + left + right >= 2)
      this.ans = root;
    return (mid + left + right > 0);
  }

}
