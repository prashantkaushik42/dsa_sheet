package Trees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import Trees.TreeNode;

/**
 * ZigZagTraversal
 */
public class ZigZagTraversal {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    List<List<Integer>> res = zigzagLevel(root);
    System.out.println(res);
  }

  static List<List<Integer>> zigzagLevel(TreeNode root) {
    Queue<TreeNode> queue = new LinkedList<>();
    List<List<Integer>> res = new ArrayList<>();
    TreeNode node;
    queue.add(root);

    while(!queue.isEmpty()){
      int qlen = queue.size();
      List<Integer> inner = new ArrayList<>();
      for(int i=0; i<qlen; i++) {
        node = queue.poll();
        inner.add(node.val);
        if(node.left != null) queue.add(node.left);
        if(node.right != null) queue.add(node.right);
      }
      
      if(inner.size() > 0) {
        if(res.size() % 2!=0) {
          Collections.reverse(inner);
        }
        res.add(inner);
      }
    }
    return res;
  }
}
