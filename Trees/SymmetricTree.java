package Trees;

import Trees.TreeNode;

public class SymmetricTree {
  public static void main(String[] args) {

  }

  static boolean isSymmetric(TreeNode root) {
    if (root == null)
      return false;
    return isSymmetricUtil(root.left, root.right);
  }

  // This is the mirror image which means the left of one node equal to the right
  // of another node and vice-versa
  static boolean isSymmetricUtil(TreeNode node1, TreeNode node2) {
    if (node1 == null || node2 == null)
      return node1 == node2;
    return (node1.val == node2.val) && isSymmetricUtil(node1.left, node2.right)
        && isSymmetricUtil(node1.right, node2.left);
  }
}
