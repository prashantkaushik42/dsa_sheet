package Trees;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import Trees.TreeNode;

/**
 * ConstructBinaryTree
 */
public class ConstructBinaryTree {
  Map<Integer, Integer> inorderPositions = new HashMap<>();
  public static void main(String[] args) {
    int[] preorder = { 3,9,20,15,7 }, inorder = { 9,3,15,20,7 };
    // TreeNode node = buildTree(preorder, inorder); 
    TreeNode node = buildTreeWithOutCopy(preorder, inorder); 
  }
  
  // Intuition here is 
  // we know that preorder tells us [root, left, right]
  // and inorder tells [left, root, right]
  // we need to leverage these
  // first element of the preorder is always the root node of the tree
  //

  static TreeNode buildTree(int[] preorder, int[] inorder) {
    if(preorder.length == 0 || inorder.length == 0) return null;

    // first element in the preorder is always be the root node
    TreeNode root = new TreeNode(preorder[0]);
    int mid = 0;
    for(int i=0; i<inorder.length; i++ ){
      // we need to check the same root value in the inorder because it tells us the left and right items
      // means that the item at the left of this will be at the left of the tree
      if(preorder[0] == inorder[i]) mid = i;
    }

    // here we copy the items which need update the preorder and inorder for the next call
    // due to which the root node will always changing 
    root.left = buildTree(Arrays.copyOfRange(preorder, 1, mid+1), Arrays.copyOfRange(inorder, 0, mid));
    root.right = buildTree(Arrays.copyOfRange(preorder, mid+1, preorder.length), Arrays.copyOfRange(inorder, mid+1, inorder.length));
    return root;
  }
  
  public TreeNode buildTreeWithOutCopy(int[] preorder, int[] inorder) {
    if(preorder.length < 1 || inorder.length < 1) return null;
    for(int i=0; i<inorder.length; i++) {
      inorderPositions.put(inorder[i], i);
    }
    return builder(preorder, 0, 0, inorder.length-1);
  }

  public TreeNode builder(int[] preorder, int preorderIndex, int inorderLow, int inorderHigh) {
    if(preorderIndex > preorder.length-1 || inorderLow > inorderHigh) return null;
    int currVal = preorder[preorderIndex];
    TreeNode n = new TreeNode(currVal);
    int mid = inorderPositions.get(currVal);
    n.left = builder(preorder, preorderIndex+1, inorderLow, mid-1);
    n.right= builder(preorder, preorderIndex+(mid-inorderLow)+1, mid+1, inorderHigh);
    return n;
  }
}
