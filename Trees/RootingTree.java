package Trees;

import java.util.LinkedList;
import java.util.List;
/**
 * RootingTree
 */
public class RootingTree {
  public static class TreeNode {
    private int id;
    private TreeNode parent;
    private List<TreeNode> children;

    public TreeNode(int id) {
      this(id, null);
    }

    public TreeNode(int id, TreeNode parent) {
      this.id = id;
      this.parent = parent;
      this.children = new LinkedList<>(); 
    }

    public void addChildren(TreeNode... nodes) {
      for(TreeNode node: nodes) {
        children.add(node);
      }
    }

    public int id() {
      return id;
    }

    public TreeNode parent() {
      return parent;
    }

    public List<TreeNode> children() {
      return children;
    }

    @Override
    public String toString() {
      return String.valueOf(id);
    }

    // checks for the id equality not the subtree 
    @Override
    public boolean equals(Object obj) {
      if(obj instanceof TreeNode) {
        return id() == ((TreeNode) obj).id();
      }
      return false;
    }
  }

  
}
