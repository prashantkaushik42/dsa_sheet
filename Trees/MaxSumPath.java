package Trees;

/**
 * MaxSumPath
 */
public class MaxSumPath {

  int maxSum = Integer.MIN_VALUE;

  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    MaxSumPath obj = new MaxSumPath();
    obj.maxSum(root);
    // System.out.println(ans);
    System.out.println(obj.maxSum);
  }

  public int maxSum(TreeNode root) {
    if (root == null)
      return 0;
    int left = Math.max(0, maxSum(root.left));
    int right = Math.max(0, maxSum(root.right));
    this.maxSum = Math.max(this.maxSum, left + right + root.val);
    return Math.max(left, right) + root.val;
  }

}
