package Trees;

// import javafx.util.Pair;
import Trees.TreeNode;
/**
 * BalancedTree
 */
public class BalancedTree {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    boolean ans = isBalancedOP(root);
    System.out.println(ans);
  }

  static boolean isBalanced(TreeNode root) {
    return height(root) != -1;
  }

  static int height(TreeNode node) {
    if(node == null) return 0;
    int leftH = height(node.left);
    if(leftH == -1) return -1;
    int rightH = height(node.right);
    if(rightH == -1) return -1;

    if(Math.abs(leftH - rightH) > 1) return -1;
    return Math.max(leftH, rightH) + 1;
  }

  static boolean isBalancedOP(TreeNode node) {
    return dfs(node).balanced;
  }

  static Pair dfs(TreeNode node) {
    if(node == null) return new Pair(true, 0);

    var left = dfs(node.left);
    var right = dfs(node.right);

    boolean balanced = left.balanced && right.balanced && (Math.abs(left.height - right.height) <= 1);
    return new Pair(balanced, 1+Math.max(left.height, right.height));
  }

  static class Pair{
    boolean balanced;
    int height;

    Pair(boolean balanced, int height) {
      this.balanced = balanced;
      this.height = height;
    }
  }
}
