package Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import Trees.TreeNode;

/**
 * LevelOrder
 */
public class LevelOrder {
  public static void main(String[] args) {
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right = new TreeNode(3);
    root.right.right = new TreeNode(7);
    root.right.left = new TreeNode(6);
    root.right.left.right = new TreeNode(8);

    // levelOrderTraversal(root);
    List<List<Integer>> res = levelOrderUsingQueue(root);
    System.out.println(res);
  }

  // This is the recursive way of level order traversal
  static void levelOrderTraversal(TreeNode root) {
    // here we take care of the height of the node
    // and try for every level
    int h = height(root);
    for (int i = 1; i <= h; i++) {
      // level starts from 1 to the height of the tree
      printCurrentLevel(root, i);
      System.out.println();
    }
  }

  static int height(TreeNode root) {
    if (root == null)
      return 0;

    int lheight = height(root.left);
    int rheight = height(root.right);

    return Math.max(lheight, rheight) + 1;
  }

  static void printCurrentLevel(TreeNode root, int level) {
    if (root == null)
      return;
    // if level == 1 means that we are at the top level or we only have
    if (level == 1)
      System.out.print(root.val + " ");
    else if (level > 1) {
      printCurrentLevel(root.left, level - 1);
      printCurrentLevel(root.right, level - 1);
    }
  }

  static List<List<Integer>> levelOrderUsingQueue(TreeNode root) {
    TreeNode curr = root;
    List<List<Integer>> res = new ArrayList<>();
    Queue<TreeNode> queue = new LinkedList<>();
    if (curr == null)
      return res;
    queue.add(curr);
    TreeNode node;

    while (!queue.isEmpty()) {
      int qLen = queue.size();
      List<Integer> level = new ArrayList<>();
      for (int i = 1; i <= qLen; i++) {
        node = queue.poll();
          level.add(node.val);
          if (node.left != null)
            queue.add(node.left);
          if (node.right != null)
            queue.add(node.right);
      }
        res.add(level);
    }
    return res;
  }
}
