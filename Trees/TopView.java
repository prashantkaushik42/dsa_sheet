package Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

/**
 * TopView
 */
public class TopView {
  static class Pair {
    Integer hd;
    Node node;

    Pair(int hd, Node node) {
      this.hd = hd;
      this.node = node;
    }
  }

  public static void main(String[] args) {
    topView(root);
  }

  static ArrayList<Integer> topView(Node root) {
    ArrayList<Integer> res = new ArrayList<>();
    if(root == null) return res;
    Queue<Pair> queue = new LinkedList<>();
    Map<Integer, Integer> map = new TreeMap<>();
    queue.add(new Pair(0, root));

    while(!queue.isEmpty()) {
      Pair it = queue.poll();
      int hd = it.hd;
      Node temp = it.node;
      if(!map.containsKey(hd)) {
        map.put(hd, temp.data);
      }
      if(temp.left != null) queue.add(new Pair(hd-1, temp.left));
      if(temp.right != null) queue.add(new Pair(hd+1, temp.right));
    }

    for(Map.Entry<Integer, Integer> entry: map.entrySet()) {
      res.add(entry.getValue());
    } 

    return res;
  }
}
