package Trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import Trees.Node;

/**
 * BottomView
 */
public class BottomView {
  public static void main(String[] args) {
    ArrayList<Integer> res = bottomView(root);
    System.out.println(res);
  }

  // APPROACH:
  //  in this we have used the horizontal distance from the root node
  //  horizontal distance means that suppose our root node is placed on the origin of the 2D (0,0) and our tree is represented
  //  if our node is at the left of the tree then we need to reduce the horizontal distance by 1 (-1) 
  //  and for right we need to add it
  static ArrayList<Integer> bottomView(Node node) {
    ArrayList<Integer> res = new ArrayList<>();
    if(node == null) return res;
    // we have to use treemap because treemap stores the keyvalue pair in the sorted manner
    Map<Integer, Integer> map  = new TreeMap<>();
    Queue<Node> queue = new LinkedList<>();
    node.hd = 0;
    queue.add(node);

    while(!queue.isEmpty()) {
      Node temp = queue.poll();
      int hd = temp.hd;
      map.put(hd, temp.data);
      if(temp.left != null) {
        temp.left.hd = hd-1;
        queue.add(temp.left);
      }
      if(temp.right != null) {
        temp.right.hd = hd+1;
        queue.add(temp.right);
      }
    }

    for(Map.Entry<Integer,Integer> entry: map.entrySet()) {
      res.add(entry.getValue());
    }
    return res;
  }
}
