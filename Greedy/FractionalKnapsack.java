package Arrays;

import java.util.Arrays;

/**
 * FractionalKnapsack
 */
public class FractionalKnapsack {
  static class Item {
    int value;
    int weight;
    Item(int value, int weight) {
      this.value = value;
      this.weight = weight;
    }
  }
  public static void main(String[] args) {
    Item[] arr = {new Item(60, 10), new Item(100, 20), new Item(120, 30)};
    double max_value = knapsack(50, arr, arr.length);
    System.out.println(max_value);
  }

  static double knapsack(int W, Item[] arr, int n) {
    // Arrays.sort(arr, (Item a,Item b) -> ((double)a.value/(double)a.weight) - ((double)b.value / (double)b.weight) );
    Arrays.sort(arr, (Item a, Item b) -> {
      double r1 = (double)a.value / (double) a.weight;
      double r2 = (double)b.value / (double) b.weight;
      return Double.compare(r2,r1);
    });
    int curr_weight = 0;
    double max_value = 0.0;
    for(Item item: arr) {
      if(curr_weight+ item.weight <= W) {
        max_value += item.value;
        curr_weight += item.weight;
      }
      else{
        int left_weight = W - curr_weight;
        double fraction = (double)item.value*(double)left_weight / (double)item.weight;
        max_value += fraction;
        break;
      }
    }
    return max_value;
  }
}
