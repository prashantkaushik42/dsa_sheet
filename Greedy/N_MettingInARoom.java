package Greedy;

import java.util.Arrays;

/**
 * N_MettingInARoom
 */
public class N_MettingInARoom {
  public static void main(String[] args) {
    int[] start = { 1, 3, 0, 5, 8, 5 };
    int[] end = { 2, 4, 6, 7, 9, 9 };
    int maxMeeting = maxMeeting(start, end, start.length);
    System.out.println(maxMeeting);
  }

  static int maxMeeting(int[] start, int[] end, int n) {
    int[][] intervals = new int[n][2];
    for (int i = 0; i < n; i++) {
      intervals[i][0] = start[i];
      intervals[i][1] = end[i];
    }

    int maxMeeting = 0;
    if (n > 0)
      maxMeeting = 1;
    int prev_index = 0;
    Arrays.sort(intervals, (a, b) -> a[1] - b[1]);
    for (int i = 1; i < n; i++) {
      if(intervals[prev_index][1] < intervals[i][0]) {
        maxMeeting++;
        prev_index = i;
      } 
    }
    System.out.println(Arrays.deepToString(intervals));
    return maxMeeting;
  }
}
