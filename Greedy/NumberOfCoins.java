package Greedy;

import java.util.Arrays;
import java.util.Collections;

/**
 * NumberOfCoins
 */
public class NumberOfCoins {
  public static void main(String[] args) {
    int V = 49;
    int[] coins = {1,2,5,10,20,50,100,500,1000};
    int min_coins = minCoins(coins, coins.length, V);
    System.out.println(min_coins);
  }

  static int minCoins(int[] coins, int M, int V) {
    int coin_count = 0;
    for(int i=M-1; i>=0; i--) {
      while(V >= coins[i]) {
        V -= coins[i];
        coin_count++;
      }
    }
    return coin_count;
  }
}
