package Greedy;

import java.util.Arrays;

/**
 * MinimumPlatform
 */
public class MinimumPlatform {
  public static void main(String[] args) {
    int[] arr = { 900, 940, 950, 1100, 1500, 1800 };
    int[] dep = { 910, 1200, 1120, 1130, 1900, 2000 };
    int platforms = findPlatforms(arr, dep, arr.length);
    System.out.println(platforms);
  }

  static int findPlatforms(int[] arr, int[] dep, int n) {
  Arrays.sort(arr);
  Arrays.sort(dep);
    int prev = 0;
    int platforms = 1;
    for (int i = 1; i < n; i++) {
      if(arr[i] > dep[prev]) prev++;
      else{
        platforms++;
      }
    }
    return platforms;
  }
}
