package Greedy;

import java.util.Arrays;

/**
 * JobScheduling
 */
public class JobScheduling {
  static class Job {
    int id;
    int profit;
    int deadline;

    Job(int id, int deadline, int profit) {
      this.id = id;
      this.deadline = deadline;
      this.profit = profit;
    }

    @Override
    public String toString() {
      String res = "";
      res = "deadline:-> " + deadline + " profit:-> " + profit;
      return res;
    }
  }

  public static void main(String[] args) {
    Job[] jobArr = {new Job(1, 4, 20), new Job(2, 1, 10), new Job(3, 1, 4), new Job(4, 1, 30)};
    int[] scheduledJobs = jobScheduling(jobArr, jobArr.length);
    System.out.println(Arrays.toString(scheduledJobs));
    for(int i=0 ;i< 4; i++) {
      System.out.println(jobArr[i].toString());
    }
  }

  static int[] jobScheduling(Job[] arr, int n) {
    boolean[] slot = new boolean[n];
    // sort in descending order of the profit
    Arrays.sort(arr, (Job j1, Job j2) -> j2.profit - j1.profit);
    int profit = 0;
    int jobCount = 0;
    for(Job job: arr) {
      //check for the deadline
      // deadline >= 1 means that time required for a job to complete is 1
      int deadline = job.deadline;
      while(deadline >= 1) {
        // check if the slot is already occupied by another job
        // deadline-1 is used for the index based
        // if the slot is already occupied (means we need to go into else statement)
        // then deadline-- to check in the prev deadline the max profit
        if(!slot[deadline-1]) {
          slot[deadline-1] = true;
          profit += job.profit;
          jobCount++;
          break;
        }
        deadline--;
      }
    }
    return new int[]{jobCount, profit};
  }
}
