import java.util.LinkedList;

import LinkedList.CustomLL;

public class ReverseLL {
  public static void main(String[] args) {
    CustomLL<Integer> list = new CustomLL<>();
    list.addLast(1);
    list.addLast(2);
    list.addLast(3);
    list.addLast(4);
    list.addLast(5);
    System.out.println(rev.toString());
  }

  static ListNode reverseList(ListNode head) {
    if(head == null) return null;
    ListNode curr = head;
    ListNode next = null;
    ListNode prev = null;

    while(curr != null) {
      next = curr.next;
      curr.next = prev;
      prev = curr;
      curr = next; 
    }

    return prev;
  }

  static ListNode reverseRec(ListNode head) {
    if(head == null || head.next == null) return head;
    ListNode node = reverseRec(head.next);
    head.next.next = head;
    head.next = null;
    return node;
  }
}
