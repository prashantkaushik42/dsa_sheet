package LinkedList;

import java.util.HashMap;
import java.util.HashSet;

import org.w3c.dom.Node;

/**
 * CopyListRandomPointer
 */
public class CopyListRandomPointer {

  public static void main(String[] args) {

  }

  static Node copyRandomList(Node head) {
    // for deep copying the list we have to passes 
    // first for to copy the items from the original list
    // second for the random node pointer
    HashMap<Node, Node> oldTocopy = new HashMap<>();
    // we have used null for random to points null
    oldTocopy.put(null, null);
    Node curr = head;
    while (curr != null) {
      Node copy = new Node(curr.val);
      oldTocopy.put(curr, copy);
      curr = curr.next;
    }

    curr = head;
    while (curr != null) {
      Node copy = oldTocopy.get(curr);
      copy.next = oldTocopy.get(curr.next);
      copy.random = oldTocopy.get(curr.random);
      curr = curr.next;
    }

    return oldTocopy.get(head);
  }
}
