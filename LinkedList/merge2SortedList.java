public class merge2SortedList {
  public static void main(String[] args) {
    
  }

  static ListNode mergeTwoSorted(ListNode list1, ListNode list2) {
    ListNode dummy = new ListNode(0);
    ListNode tail = dummy;

    while(list1 != null && list2 != null) {
      if(list1 < list2) {
        tail.next = list1;
        list1 = list1.next;
        tail = tail.next;
      }
      else{
        tail.next = list2;
        list2 = list2.next;
        tail = tail.next;
      }
    }

    while(list2 != null) {
        tail.next = list2;
        list2 = list2.next;
        tail = tail.next;
    }

    while(list2 != null) {
      tail.next = list1;
      list1 = list1.next;
      tail = tail.next;
    }

    return dummy.next;
  }
}
