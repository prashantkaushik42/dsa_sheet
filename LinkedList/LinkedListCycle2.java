package LinkedList;

/**
 * LinkedListCycle2
 */
public class LinkedListCycle2 {
  static ListNode head = null;
  static ListNode tail = null;
  static int size = 0;

  static class ListNode {
    int val;
    ListNode next;

    public ListNode(int val) {
      this.val = val;
    }

    public ListNode(int val, ListNode next) {
      this.val = val;
      this.next = next;
    }

    public void addLast(int val) {
      if (isEmpty()) {
        head = tail = new ListNode(val);
      } else {
        ListNode node = new ListNode(val);
        tail.next = node;
        tail = node;
      }
      size++;
    }

    public boolean isEmpty() {
      if (size == 0)
        return true;
      return false;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("[ ");
      ListNode temp = head;
      while (temp != null) {
        sb.append(temp.val);
        if (temp.next != null)
          sb.append(", ");
        temp = temp.next;
      }
      sb.append(" ]");
      return sb.toString();
    }
  }

  public static void main(String[] args) {

  }

  static ListNode detectCycle(ListNode head) {
    ListNode slow = head;
    ListNode fast = head;
    while (fast != null && fast.next != null) {
      slow = slow.next;
      fast = fast.next.next;
      if (slow == fast)
        break;
    }

    if (fast == null || fast.next == null)
      return null;
    slow = head;
    while (slow != fast) {
      slow = slow.next;
      fast = fast.next;
    }
    return slow;
  }
}
