## Hello world

- this is just an example
- I am working on it
  - hello
  - **world**
    [Visit github](https://www.github.com)

![Landscape](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallup.net%2Fwp-content%2Fuploads%2F2016%2F01%2F137302-nature-Canada-landscape-lake-forest-mountain.jpg&f=1&nofb=1&ipt=e14104c6563e62945c693db5d1fd9a5d8d633d9e798f66de7ac5dbf5acfbfd72&ipo=images)

> His words seemed to have struck some deep chord in his own nature. Had he spoken
> of himself, of himself as he was or wished to be? Stephen watched his face for some
> moments in silence. A cold sadness was there. He had spoken of himself, of his own
> loneliness which he feared.
>
> —Of whom are you speaking? Stephen asked at length.
>
> Cranly did not answer.

- apple
- banana

1. Apple
2. Banana

```java
class Main{
  public static void main(String[] args) {
    System.out.println("Hello world")
  }
}
```
