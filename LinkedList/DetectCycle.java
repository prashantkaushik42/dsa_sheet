package LinkedList; 

/**
 * DetectCycle
 */
public class DetectCycle {

  static ListNode head = null;
  static int size = 0;
  static ListNode tail = null;
  static class ListNode{
    int val;
    ListNode next;

    public ListNode(int val) {
      this.val = val;
      this.next = null;
    }

    public void addLast(int val) {
      if(isEmpty()) {
        head = tail = new ListNode(val);
      }
      else{
        ListNode temp = new ListNode(val);
        tail.next = temp;
        tail = tail.next;
      }
      size++;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("[ ");
      ListNode trav = head;
      while(trav != null) {
        sb.append(trav.val);
        if(trav.next != null) sb.append(", ");
        trav = trav.next;
      }
      sb.append(" ]");
      return sb.toString();
    }

    public ListNode getNode() {
      return head;
    }

    public boolean isEmpty() {
      return size == 0 ? true : false;
    }
  }


  public static void main(String[] args) {
    ListNode a = new ListNode(10);
    a.addLast(1);
    a.addLast(2);
    a.addLast(3);
    a.addLast(4);
    a.addLast(5);
    boolean detect = hasCycle(head);
    System.out.println(a.toString());
    System.out.println(detect);
  }

  static boolean hasCycle(ListNode head) {
    ListNode fast = head;
    ListNode slow = head;

    while (fast != null && fast.next != null) {
      fast = fast.next.next;
      slow = slow.next;

      if(fast == slow) return true;
    }
    return false;
  }

}
