public class middleOfLL {
  public static void main(String[] args) {
    
  }

  // Use of slow and fast pointers
  // fast pointer moves 2 steps at a time and slow moves only one step
  // whenever fast is pointing to the last means that slow will always be at the middle
  static ListNode middleNode(ListNode head) {
    if(head == null) return null;
    ListNode fast = head;
    ListNode slow = head;

    while(fast != null && fast.next != null) {
      fast = fast.next.next;
      slow = slow.next;
    }

    return slow;
  }
}
