package LinkedList;

import LinkedList.CustomLL;
import LinkedList.CustomLL.Node;

/**
 * RotateRight
 * length == k
 * k == 0
 * k==1
* lenth == 1
 * length is a multiple of k
 */


public class RotateRight {
  public static void main(String[] args) {
  }

  static Node rotateRight(Node head, int k) {
    if(head == null || k == 0) return head;
    int length = 0;
    Node trav = head;
    Node tail = head;
    while (trav != null) {
      trav = trav.next;
      if (trav != null) {
        tail = trav;
      }
      length++;
    }

    int leftLength = length - k%length;
    if(length == leftLength) return head;
    int currLen = 0;
    trav = head;
    while (currLen < leftLength) {
      trav = trav.next;
      currLen++;
    }

    Node next = trav.next;
    trav.next = null;
    tail.next = head;
    return next;
  }
}
