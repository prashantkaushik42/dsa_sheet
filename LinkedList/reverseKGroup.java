package LinkedList;

import LinkedList.CustomLL;
import LinkedList.CustomLL.ListNode;

/**
 * reverseKGroup
 */
public class reverseKGroup {

  public static void main(String[] args) {
    CustomLL<> list = new CustomLL<>();
    list.addLast(1);
    list.addLast(2);
    list.addLast(3);
    list.addLast(4);
    list.addLast(5);
    ListNode head = list.getHead();
    ListNode node = reverseByGroups(head, 2);
    System.out.println(list.toString());
  }

  static ListNode<> reverseByGroups(ListNode<> head, int k) {
    ListNode dummy = new ListNode(0, head);
    ListNode groupPrev = dummy;

    while (true) {
      ListNode kthNode = getKth(groupPrev, k);
      if (kthNode == null)
        break;
      ListNode groupNext = kthNode.next;

      // reverse group
      ListNode prev = kthNode.next;
      ListNode curr = groupPrev.next;
      while (curr != groupNext) {
        ListNode tmp = curr.next;
        curr.next = prev;
        prev = curr;
        curr = tmp;
      }

      ListNode tmp = groupPrev.next;
      groupPrev.next = kthNode;
      groupPrev = tmp;
    }
    return dummy.next;
  }

  static ListNode getKth(ListNode curr, int k) {
    while (curr != null && k > 0) {
      curr = curr.next;
      k -= 1;
    }
    return curr;
  }
}
