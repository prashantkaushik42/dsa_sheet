package LinkedList;

import java.util.*;

/**
 * isPalindrome
 */
public class isPalindrome {
  static ListNode head = null;
  static ListNode tail = null;
  static int size = 0;

  static class ListNode {
    int val;
    ListNode next;

    public ListNode(int val) {
      this.val = val;
    }

    public ListNode(int val, ListNode next) {
      this.val = val;
      this.next = next;
    }

    public void addLast(int val) {
      if (isEmpty()) {
        head = tail = new ListNode(val);
      } else {
        ListNode node = new ListNode(val);
        tail.next = node;
        tail = node;
      }
      size++;
    }

    public boolean isEmpty() {
      if (size == 0)
        return true;
      return false;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("[ ");
      ListNode temp = head;
      while (temp != null) {
        sb.append(temp.val);
        if (temp.next != null)
          sb.append(", ");
        temp = temp.next;
      }
      sb.append(" ]");
      return sb.toString();
    }
  }

  public static void main(String[] args) {
    isPalindrome.ListNode list = new ListNode(1);
    list.addLast(1);
    list.addLast(2);
    // list.addLast(2);
    // list.addLast(1);
    boolean check = isPal(head);
    System.out.println(check);
    System.out.println(list.toString());
  }

  static boolean isPal(ListNode head) {
    ListNode mid = getMid(head);
    ListNode secondHead = reverseList(mid);

    while (head != null && secondHead != null) {
      if (head.val != secondHead.val)
        return false;
      head = head.next;
      secondHead = secondHead.next;
    }
    return true;
  }

  static ListNode getMid(ListNode head) {
    ListNode fast = head;
    ListNode slow = head;
    while (fast != null && fast.next != null) {
      fast = fast.next.next;
      slow = slow.next;
    }
    return slow;
  }

  static ListNode reverseList(ListNode head) {
    ListNode prev = null;
    ListNode curr = head;
    ListNode next = null;

    while (curr != null) {
      next = curr.next; 
      curr.next = prev;
      prev = curr;
      curr = next;
    }
    return prev;
  }
}
