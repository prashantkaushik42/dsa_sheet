public class removeNthNode {
  // remove Nth node from last
  // here we have used two pointers approach
  // where our first pointer is n steps ahead
  // then start slow and fast pointer with same rate and whenever fast pointer reaches end of the list 
  // then always our slow pointer is at the node we want to delete
  static ListNode removeOP(ListNode head, int n) {
    // here we have used an extra node because whenever our slow pointer reached to the node we want to delete 
    // then our fast pointer is at the null
    // so to avoid it we have inserted an extra node at start of the List
    ListNode start = new ListNode();
    start.next = head;
    ListNode fast = start;
    ListNode slow = start;

    for(int i=1; i<= n; i++) {
      fast = fast.next;
    }

    while(fast != null) {
      fast = fast.next;
      slow = slow.next;
    }

    slow.next = slow.next.next;
    return start.next;
  }
}
