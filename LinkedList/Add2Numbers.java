/**
 * Add2Numbers
 */
public class Add2Numbers {
   public static void main(String[] args) {
        
   } 

   static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
       // here we have taken a dummmy node 
        ListNode dummy = new ListNode(-1);
        ListNode curr = dummy;

        ListNode cur1 = l1;
        ListNode cur2 = l2;
        int carry = 0;
        while(cur1 != null || cur2 != null || carry != 0) {
            // check if the value exists or not if not exists then put val to 1  
            int num1 = cur1 == null ? 0 : cur1.val;
            int num2 = curr2 == null ? 0 : curr2.val;
            
            // sum it with the carry
            int sum = num1+num2 +carry;
            int newNum = sum %10;
            carry = sum/10;
            // add a new node to the next of the curr node
            curr.next = new ListNode(newNum);
            curr = curr.next;

            cur1 = cur1 == null ? null : cur1.next;
            cur2 = cur2 == null ? null : cur2.next;
        }
        return dummy.next;
   }
}
