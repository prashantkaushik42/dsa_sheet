import java.util.HashSet;

/**
 * IntersectionInTwoLinkedList
 */
public class IntersectionInTwoLinkedList {

    public static void main(String[] args) {
        
    }

    // This is an O(m * n) solution where m and n is the size of the linked lists
    static ListNode intersectionPresent(ListNode headA, ListNode headB) {
        while (headB != null) {
           ListNode temp = headA; 
           while(temp != null) {
               if(temp == headB) return headB;
               temp = temp.next;
           }
           headB = headB.next;
        }
        return null;
    }

    static intersectionHash(ListNode headA, ListNode headB) {
        HashSet<ListNode> set = new HashSet<>();
        while(headA != null) {
            set.add(headA);
            headA = headA.next;
        }

        while(headB != null) {
            if(set.contains(headB)) return headB;
            headB = headB.next;
        }
        return null;
    }
}
