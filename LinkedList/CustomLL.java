package LinkedList;

public class CustomLL<T> {
  private int size = 0;
  private Node<T> head = null;
  private Node<T> tail = null;

  public static class Node<T> {
    public T data;
    public Node<T> next;
    
    public Node(T data) {
      this.data = data;
      this.next = null;
    }

    @Override
    public String toString() {
      return data.toString();
    }
  }

  // Empty the linked list O(n)
  public void clear() {
    Node<T> trav = head;
    while(trav != null) {
      Node<T> next = trav.next;
      trav.next = null;
      trav.data = null;
      trav = next;
    }
    head = tail = null;
    size = 0;
  }

  // return the size of the linked list
  public int size() {
    return this.size;
  }

  // check if the linked list is empty or not
  public boolean isEmpty() {
    return size() == 0;
  }

  // add the element the starting of the linked list
  public void addFirst(T elem) {
    if(isEmpty()) {
      head = tail = new Node<T>(elem);
    }
    else {
      Node<T> temp = new Node<T>(elem);  
      temp.next = head.next;
      head = temp.next;
    }
    size++;
  }

  public void addLast(T elem) {
    if(isEmpty()) {
      head = tail = new Node<T>(elem);
    }
    else {
      Node<T> temp = new Node<T>(elem);
      tail.next = temp;
      tail = tail.next;
    }
    size++;
  }

  public T peekFirst() {
    if(isEmpty()) throw new RuntimeException("Empty List");
    return head.data;
  }

  public T peekLast() {
    if(isEmpty()) throw new RuntimeException("Empty List");
    return tail.data;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[ ");
    Node<T> trav = head;
    while(trav != null) {
      sb.append(trav.data);
      if(trav.next != null) {
        sb.append(", ");
      }
      trav = trav.next;
    }
    sb.append(" ]");
    return sb.toString();
  }

  public Node getHead() {
    return this.head;
  }
}
