> **Here we need to remove the nth node from last**

![Remove nth Node leetcode](https://assets.leetcode.com/uploads/2020/10/03/remove_ex1.jpg)

### Intuition

##### Approach 1:

- let suppose total length of a linked list is N;
- we need to remove nth from last
- so from start it would be N-n
