package LinkedList;

/**
 * FlattenLinkedList
 */
public class FlattenLinkedList {

  public static void main(String[] args) {
    
  }

  static Node flatten(Node head) {
    if(head == null || head.next == null) return head;
    head.next = flatten(head.next);
    head = mergeTwoLists(head, head.next);
    return head;
  }

  static Node mergeTwoLists(Node curr, Node right) {
    Node temp = new Node(0);
    Node res = temp;

    while(curr != null && right != null) {
      if(curr.data < right.data) {
        temp.bottom = curr;
        temp = temp.bottom;
        curr = curr.bottom;
      }
      else{
        temp.bottom = right;
        temp = temp.bottom;
        right = right.bottom;
      }
    }
    if(curr != null) temp.bottom = curr;
    else temp.bottom = right;
    return res.bottom;
  }
}
