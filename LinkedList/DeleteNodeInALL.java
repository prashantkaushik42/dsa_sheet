/**
 * DeleteNodeInALL
 */
public class DeleteNodeInALL {
    public static void main(String[] args) {
        
    }
   
static void deleteNode(ListNode node) {
    // node should not be the last node 
    // hence nextNode will return something
    ListNode nextNode = node.next;
    node.val = nextNode.val;
    node.next = nextNode.next;
    nextNode.next = null;
}

}
