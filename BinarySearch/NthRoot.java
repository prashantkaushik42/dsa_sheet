package BinarySearch;

/**
 * NthRoot
 */
public class NthRoot {
  public static void main(String[] args) {
    int n = 3;
    int m = 27;
    double ans = findRoot(n, m);
    System.out.println("Using custom function " + ans);
    System.out.println("Using pow function " + Math.pow(m, 1.0 / n));
  }

  static double findRoot(int n, int m) {
    double lo = 1.0;
    double hi = (double) m;
    double eps = 1e-7;

    while (true) {
      double mid = (lo + hi) / 2.0;
      if (nMultiply(mid, n) == m)
        return mid;
      else if (nMultiply(mid, n) < m) {
        lo = mid;
      } else
        hi = mid;

      if ((double) ((int) (lo * 10000000)) / 10000000 == (double) ((int) (hi * 10000000)) / 10000000) {
        break;
      }
    }
    return lo;
  }

  static double nMultiply(double number, int n) {
    double ans = 1.0;
    for (int i = 1; i <= n; i++) {
      ans = ans * number;
    }
    return ans;
  }
}
