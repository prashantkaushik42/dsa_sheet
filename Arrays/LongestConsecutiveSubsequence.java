import java.util.HashSet;

public class LongestConsecutiveSubsequence {
  public static void main(String[] args) {
    int[] nums = {0,3,7,2,5,8,4,6,0,1};
    int longestStreak = longestConsecutive(nums);
    System.out.println(longestStreak);
  }

  //Intution is we have to think it as a number line 
  //where there is the break in number line then we need to find the count 
  static int longestConsecutive(int[] nums) {
    HashSet<Integer> set = new HashSet<>();
    int longestStreak = 0;
    for(int num: nums) {
      if(!set.contains(num)) set.add(num);
    }

    for(int i=0; i<nums.length; i++) {
      // if the previous element is not found mens that this will be the starting of the number line's block
      // if prev is found which means that we have already calculated it in the previous block
      if(!set.contains(nums[i] -1)) {
        int currentStreak = 0;

        // by adding the currentStreak we find the next element from here where the block does not breaks
        while(set.contains(nums[i] + currentStreak)) {
          currentStreak++;
        }
        longestStreak = Math.max(currentStreak, longestStreak);
      }
    }
      return longestStreak;
  }
}
