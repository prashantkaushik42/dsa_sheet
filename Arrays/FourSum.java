// package Arrays;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSum {
  public static void main(String[] args) {
     int[] nums = {1,0,-1,0,-2,2};
     int target = 0;
     List<List<Integer>> res = sum(nums, target);
     System.out.println(res);
  }

  static List<List<Integer>> sum(int[] nums, int tar) {
    List<List<Integer>> res = new ArrayList<>();
    if(nums == null || nums.length == 0) return res;
    
    long target = (long) tar;

    int n = nums.length;
    // sort the array and use two pointers
    Arrays.sort(nums);
    
    for (int i = 0; i < n; i++) {
      // fixing the first element (i) and change the target 
      long target_3 = target - nums[i];
      for (int j = i+1; j < n; j++) {
        // fixing the second element
        long target_2 = target_3 - nums[j];
        
        int front = j+1;
        int back = n-1;
        while (front < back) {
          // use two pointers
          long two_sum = nums[front] + nums[back];
          if(two_sum < target_2) front++;
          else if(two_sum > target_2) back--;
          else {
            List<Integer> temp = new ArrayList<>();
            temp.add(nums[i]);
            temp.add(nums[j]);
            temp.add(nums[front]);
            temp.add(nums[back]);
            res.add(temp);

            // we get the same element next to it then we have to skip that particular element
            while (front < back && nums[front] == temp.get(2)) front++;
            while (front < back && nums[back] == temp.get(3)) back--;
          }
        }
        while (j+1 < n && nums[j+1] == nums[j]) j++; 
      }
      while(i+1 < n && nums[i+1] == nums[i]) i++;
    }
    return res;
  }
}
