package Arrays;

import java.util.Arrays;

/**
 * MaxConsecutiveOnes
 */
public class MaxConsecutiveOnes {
  public static void main(String[] args) {
   int[] nums = {1,0,1,1,0,1};
   int res = findMaxOnes(nums);
   System.out.println(res);
  }
  
  static int findMaxOnes(int[] nums) {
    int max_count = 0;
    int curr_count = 0;
      
    for(int i=0; i<nums.length; i++) {
      if(nums[i] == 0) curr_count = 0;
    
      else{
        curr_count++;
        max_count = Math.max(curr_count, max_count);
      }
    }
    return max_count;
  }
}
