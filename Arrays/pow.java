public class pow {
  public static void main(String[] args) {
    double x = 2.10000; 
    int n = 3;
    double ans = myPow(x, n);
    System.out.println(ans);
  }

  static double myPow(double x, int n) {
    double ans = 1.0;
    long nn = n;
    if(n < 0) nn = -nn;
    while(nn > 0) {
      if (nn % 2 == 1) {
        ans *= x;
        nn -= 1;
      }
      else {
        x = x*x;
        nn /= 2;
      }
    }
    if(n < 0) ans = (1.0) / ans;
    return ans;
  }
}
