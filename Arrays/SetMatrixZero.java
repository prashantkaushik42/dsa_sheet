import java.util.*;

public class SetMatrixZero {
  public static void main(String[] args) {
    int[][] matrix = {
    {1,1,1},{1,0,1},{1,1,1} 
    }; 
      
    setZeros(matrix);
    System.out.println(Arrays.deepToString(matrix));
  }

  static void setZeros(int[][] matrix) {
    int rows = matrix.length;
    int cols = matrix[0].length;
    
    int[] row = new int[rows];
    int[] col = new int[cols];
    Arrays.fill(row, 1);
    Arrays.fill(col, 1);

    for(int i=0; i<rows; i++) {
      for(int j=0; j<cols; j++) {
        if(matrix[i][j] == 0) {
          row[i] = 0;
          col[j] = 0;
        }
      }
    }

    for(int i=0; i<rows; i++) {
      for (int j = 0; j < cols; j++) {
        if(row[i] == 0 || col[j] == 0) 
          matrix[i][j] = 0; 
      }
    }
  }
}
