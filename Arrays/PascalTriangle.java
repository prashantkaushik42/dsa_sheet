import java.util.*;

public class PascalTriangle {
  public static void main(String[] args) {
    int numRows = 5;   
    List<List<Integer>> res = generate(numRows);
    System.out.println(res);
  }

  static List<List<Integer>> generate(int numRows) {
    List<List<Integer>> outer = new ArrayList<>();
    
    if(numRows == 0) return outer;

    // Add [1] in the list
    outer.add(new ArrayList<>(Arrays.asList(1)));

    // run the loop numRows times (means for rows)
    for(int i=1; i<numRows; i++){
      List<Integer> inner = new ArrayList<>();
      // add 1 to the start and the end of the list
      inner.add(1);
      // take the last item in the outer as prev 
      List<Integer> prev = outer.get(outer.size()-1);
      for(int j=0; j < prev.size()-1; j++) {
        // add two consecutive items in the prev and append them to the inner list 
        inner.add(prev.get(j) + prev.get(j+1));
      }
      // end of the list
      inner.add(1);
      outer.add(inner);
    }
    return outer;
  }
}
