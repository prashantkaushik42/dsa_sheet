import java.util.*;

// Kadane's Algorithm
public class MaximumSubArraySum {
  public static void main(String[] args) {
    int[] nums = {-2,1,-3,4,-1,2,1,-5,4};
    int max_sum = maxSubArray(nums);
    System.out.println(max_sum);
  }

  static int maxSubArray(int[] nums) {
    int max = Integer.MIN_VALUE;
    int curr = 0;

    for(int el: nums) {
      // add the curr element to the curr sum 
      curr += el;
      // take max of them
      max = Math.max(curr, max);
      // if curr<0 means that it will not contribute to make the sum max else it will decrease the sum
      // so if we found that then curr = 0 
      if(curr < 0) curr = 0;
    }

    return max;
  }
}
