import java.util.*;

public class majorityElement2 {
  public static void main(String[] args) {
    int[] nums = {3,2,3};
    List<Integer> list = major(nums);
    System.out.println(list);
  }

  static List<Integer> major(int[] nums) {
    int len = nums.length;
    int count1 = 0, count2 = 0, num1 = 0, num2 = 1;

    for (int num : nums) {
      if(num1 == num) count1++;
      else if(num2 == num) count2++;
      else if(count1 == 0) {
        num1 = num;
        count1 = 1;
      }
      else if(count2 == 0) {
        num2 = num;
        count2 = 1;
      }
      else {
        count1 -=1; count2 -=1;
      }
    }

    List<Integer> res = new ArrayList<>();
    count1 = 0; count2=0;

    for (int num : nums) {
      if(num == num1) count1++;
      else if(num == num2) count2++;
    }

    if(count1 > len/3) res.add(num1);
    if(count2 > len/3) res.add(num2);

    return res;
  }
}
