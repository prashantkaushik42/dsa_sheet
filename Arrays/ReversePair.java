import java.util.*;

 public class ReversePair{

   public static void main(String[] args) {
     int[] nums = {1,3,2,3,1};
     int inv = mergeSort(nums, 0, nums.length-1);
     System.out.println(inv);
   }

   // here we have used mergeSort in finding the reverse pairs 
  static int mergeSort(int[] arr, int l, int r) {
    int ans = 0;
    if(l<r) {
      int mid = l + (r-l)/2;
      ans += mergeSort(arr, l, mid);
      ans += mergeSort(arr, mid+1, r);
      ans += merge(arr, l, mid, r);
    }
    return ans;
  }

  static int merge(int[] arr, int l, int m, int r) {
    int n1 = m-l+1, n2 = r-m;
    int res = 0;
    int[] left = new int[n1];
    int[] right = new int[n2];
    
    // whenever we meet  the condition of arr[i] > 2*arr[j]
    // we need to increment the j
     int p = m + 1;
        for(int i = l;i<=m;i++) {
          // we typecast it into long because may be by multiplying by 2 it will surpasses the integer range
            while(p<= r && arr[i] > (2 * (long) arr[p])) {
                p++;
            }
            res += (p - (m+1));
        }

    for (int i = 0; i < n1; i++) {
      left[i] = arr[l+i];
    }

    for (int i = 0; i < n2; i++) {
      right[i] = arr[m+i+1];
    }

    int i=0, j=0, k=l;
    while(i<n1 && j<n2) {
      if(left[i] <= right[j]) arr[k++] = left[i++];
      else{
        arr[k++] = right[j++];
      }
    }

    while(i<n1)  arr[k++] = left[i++];
    while(j<n2) arr[k++] = right[j++];

    return res;
  }
}



