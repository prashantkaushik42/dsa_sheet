import java.util.*;

public class NextPermutation {
  public static void main(String[] args) {
    int[] nums = { 1, 2, 3 };
    next(nums);
    System.out.println(Arrays.toString(nums));
  }

  // Intuition:
  // traverse from last whenever we found a item which is smaller than the last
  // item (put i pointer here)
  // and again traverse from last if (i >= 0) then if I found an item smaller than
  // ith we need to swap i and j
  static void next(int[] nums) {
    int i = nums.length - 2;
    if (nums == null || nums.length <= 1)
      return;
    while (i >= 0 && nums[i] >= nums[i + 1])
      i--;
    // if i<0 this means that my array is already sorted in reverse order so the
    // next permutaion is the sorted in ascending order
    if (i >= 0) {
      int j = nums.length - 1;
      // check for the greater element from last
      // then swap it 
      while (nums[j] <= nums[i])
        j--;
      swap(nums, i, j);
    }
    // reverse the i+1 element
    reverse(nums, i + 1, nums.length - 1);
  }

  static void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
  }

  static void reverse(int[] nums, int i, int j) {
    while (i < j)
      swap(nums, i++, j--);
  }
}
