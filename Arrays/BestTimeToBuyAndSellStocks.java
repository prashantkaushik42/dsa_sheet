import java.util.*;

public class BestTimeToBuyAndSellStocks {
  public static void main(String[] args) {
    int[] prices = {7,1,5,3,6,4};
    // here max profit is when I buy the stock at rate 1 and sell it at rate 6 max_profit = (6-1 = 5)
    int profit = maxProfit(prices);
    System.out.println(profit);
  }

  // check if whether I can make profit or not
  static int maxProfit(int[] prices) {
    int profit = 0;
    int max_profit = 0;
    int buy = prices[0];

    for(int sell: prices) {
      // if sell - buy < 0 means profit < 0 then i will not buy the stock at the buy price
      // means that the curr sell price is smaller than the buy price so I can buy it at curr price (curr sell price)
      // to avoid the loss
      if(sell - buy < 0) buy = sell;
      else {
        // else if I got the greater number then sell hence here I need to find the max of all the profits that I got
        profit = sell - buy;
        max_profit = Math.max(profit, max_profit);
      }
    }
    return max_profit;
  }
}
