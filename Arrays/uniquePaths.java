import java.util.Arrays;

public class uniquePaths {
  public static void main(String[] args) {
    int m = 3, n=7;
    int paths = uniqPathOP(m, n);
    System.out.println(paths);
  }
  
  // this is a DP solution 
  // here we firstly initialize the first row and col as 1 means the no of steps taken to move through the right or down 
  // in first row or col is always 1
  static int uniqPath(int m, int n) {
    int[][] dp = new int[m][n];
    for (int i = 0; i < m; i++) {
      for(int j=0; j<n; j++) {
        if(i == 0 || j == 0) dp[i][j] = 1;
      }   
    }

    // for curr position the no. of ways to travel to curr position is always = steps from top + steps from left 
    for (int i = 1; i < m; i++) {
      for (int j = 1; j < n; j++) {
        dp[i][j] = dp[i-1][j] + dp[i][j-1]; 
      } 
    }

    return dp[m-1][n-1];
  }

  // This is the more optimized solution
  // Intuition: we always have to move m-1 steps rightward and n-1 downward
  // total no of steps = m+n-2;
  // we need to choose m-1 or n-1 steps from m+n-2 steps to reach to the end
  // m+n-2 C m-1 (C means combination in mathematics from permuation and combination)
  static int uniqPathOP(int m, int n) {
    int N = m+n-2;
    int r = m-1;
    double res = 1.0;
    for(int i=1; i<=r; i++) {
      // res is used to calculate the combination
      res = res * (N-r+i)/ i;
    }
    return (int)res;
  }
}
