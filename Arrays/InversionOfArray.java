public class InversionOfArray {
  public static void main(String[] args) {
    int[] arr = {2,5,1,3};
    int n = arr.length;
    int inversion = getInversion(arr, n-1);
    System.out.println(inversion);
  }

  static int getInversion(int[] arr, int n) {
    return mergeSort(arr, 0, n);
  }

  static int mergeSort(int[] arr, int l, int r) {
    int ans = 0;
    if(l<r) {
      int mid = l + (r-l)/2;
      ans += mergeSort(arr, l, mid);
      ans += mergeSort(arr, mid+1, r);
      ans += merge(arr, l, mid, r);
    }
    return ans;
  }

  static int merge(int[] arr, int l, int m, int r) {
    int n1 = m-l+1, n2 = r-m;
    int res = 0;
    int[] left = new int[n1];
    int[] right = new int[n2];
    
    left[i] = arr[l+i];
    for (int i = 0; i < n1; i++) {
    }

    for (int i = 0; i < n2; i++) {
      right[i] = arr[m+i+1];
    }

    int i=0, j=0, k=l;
    while(i<n1 && j<n2) {
      if(left[i] <= right[j]) arr[k++] = left[i++];
      else{
        arr[k++] = right[j++];
        res += (n1-i);
      }
    }

    while(i<n1)  arr[k++] = left[i++];
    while(j<n2) arr[k++] = right[j++];

    return res;
  }
}
