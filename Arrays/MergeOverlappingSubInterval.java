import java.util.*;

public class MergeOverlappingSubInterval {
  public static void main(String[] args) {
    int[][] intervals = {
    {1,3}, {2,6},{8,10},{15, 18}
    };
    
    int[][] res = merge(intervals);
    System.out.println(Arrays.deepToString(res));
  }

  // Firstly sort the array based on the first element (0th index) because after merging it will automatically be sorted
  static int[][] merge(int[][] intervals) {
    Arrays.sort(intervals, (a,b) -> a[0] - b[0]);
    LinkedList<int[]> merged = new LinkedList<>();

    for(int[] interval: intervals) {
      // if merged is empty or the last element in merged is smaller than the first element in curr interval
      // means that there will be no overlapping
      // then we need to append that interval to the list
      // else we need to modify the last element's last index of merged to the max of last element of interval of merged
      if(merged.isEmpty() || merged.getLast()[1] < interval[0]) merged.add(interval);
      else merged.getLast()[1] = Math.max(interval[1], merged.getLast()[1]);
    }

    return merged.toArray(new int[merged.size()][]);
  }
}
