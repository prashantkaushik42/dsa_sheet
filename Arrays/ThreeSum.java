package Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {
  public static void main(String[] args) {
    int[] nums = {-1,0,1,2,-1,-4};
    List<List<Integer>> res = threeSum(nums);
    System.out.println(res);
  }

  static List<List<Integer>> threeSum(int[] nums){
    List<List<Integer>> res = new ArrayList<>();
    Arrays.sort(nums);

    for(int i=0; i<nums.length; i++) {
      int a = nums[i];
      if(i >0 && a==nums[i-1]) continue;

      int l=i+1;
      int r = nums.length-1;
      while(l<r) {
        int three_sum = a+nums[l]+nums[r];
        if(three_sum > 0) r--;
        else if(three_sum < 0) l++;
        else{
          res.add(Arrays.asList(a, nums[l], nums[r]));
          l++;
          while(nums[l] == nums[l-1] && l<r) {
            l++;
          } 
        }
      }
    }
    return res;
  }
}
