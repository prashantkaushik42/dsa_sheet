import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class LargestSubArrayWith0Sum {
  public static void main(String[] args) {
    int[] arr = {15,-2,2,-8,1,7,10,23};
    int n = arr.length;
    int maxLen = maxLen(arr, n);
    System.out.println(maxLen);
  }

  // Intution:  sum of (i,j) = S, sum(i,x) = S, then sum(x+1, j) should also be S
  static int maxLen(int[] nums, int n) {
    HashMap<Integer, Integer> map = new HashMap<>();
    int sum = 0;
    int maxi = 0;
    
    // if we got this sum before this means that the sum between then prev and curr is 0
    for(int i=0; i<n; i++) {
      sum += nums[i];

      // if sum == 0
      if(sum == 0) maxi = i+1;

      else {
        // if map contains sum means that the sum is already present
        // and the sum between the prev position and curr position is always 0
        if(map.containsKey(sum)) {
          maxi = Math.max(maxi, i-map.get(sum));
        }
        else{
          map.put(sum, i);
        }
      }
    }
    return maxi;
  }
}
