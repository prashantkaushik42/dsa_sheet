public class searchMatrix {
  public static void main(String[] args) {
    int[][] matrix = {
      {1,3,5,7},
      {10, 11, 16, 20},
      {23, 30, 34, 60}
    };
    int target = 3;

   boolean isPresent = search2(matrix, target); 
   System.out.println(isPresent);
  }

  // Solution - 1
  // Array is sorted
  // here we need to start the i from the 0 index and j from n-1
  // if curr < target means we need to go down
  // if curr > target means we need to go left
  // else return true
  static boolean search1(int[][] matrix, int target) {
    int m = matrix.length;
    int n = matrix[0].length;
    int i=0;
    int j=n-1;

    while (i<m && j>=0) {
      if(matrix[i][j] < target) i++;
      else if(matrix[i][j] > target) j--;
      else return true;
    }
    return false;
  }

  // we treated the matrix (2D array) as 1D array
  // lo = 0
  // hi = n*m-1
  // and calculate the mid
  // matrix[mid / cols][mid % cols] is the curr element

  static boolean search2(int[][] matrix, int target) {
    int lo = 0;
    int m = matrix.length;
    int n = matrix[0].length;
    int hi = n*m -1;

    while (lo <= hi) {
      int mid = lo + (hi - lo) /2; 
      if(matrix[mid/n][mid%n] == target) return true;
      if(matrix[mid/n][mid%n] < target) lo = mid+1;
      else hi = mid-1;
    }
    return false;
  }
}
