package Arrays;

/**
 * TrappingRainWater
 */
public class TrappingRainWater {
  public static void main(String[] args) {
    int[] heights = { 0,1,0,2,1,0,1,3,2,1,2,1 };
    int trappedWater = trapOP(heights);
    System.out.println(trappedWater);
  }

  // LGE -> left greater element
  // RGE -> right greater element
  // here we take care of the greater element at its left and right 
  // min of both RGE and RGE will required for the water to be trapped
  static int trap(int[] heights) {
    int n = heights.length; 
    int[] LGE = new int[n];
    int[]RGE = new int[n];

    // for LGE
    int maxEl = heights[0];
    LGE[0] = heights[0];
    for(int i=1; i< n; i++) {
      maxEl = Math.max(maxEl, heights[i]);
      LGE[i] = maxEl;
    }

    maxEl = heights[n-1];
    RGE[n-1] = heights[n-1];
    for(int i=n-2; i>=0 ;i--) {
      maxEl = Math.max(maxEl, heights[i]);
      RGE[i] = maxEl;
    }

    int trappedWater = 0;
    for(int i=0; i<n; i++) {
      trappedWater = Math.min(RGE[i], LGE[i]) - heights[i];
    }
    return trappedWater;
  }

  static int trapOP(int[] height) {
    int n = height.length;
    if(n == 0) return 0;

    int l=0, leftMax = height[l], r = n-1, rightMax = height[l];
    int trappedWater = 0;
    while(l<r) {
      if(leftMax < rightMax) {
        l++;
        leftMax = Math.max(leftMax, height[l]);
        trappedWater += leftMax - height[l];
      }
      else{
        r--;
        rightMax = Math.max(rightMax, height[r]);
        trappedWater += rightMax - height[r];
      }
    }
    return trappedWater;
  }
}
