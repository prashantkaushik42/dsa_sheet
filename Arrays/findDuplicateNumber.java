// Number should be 1 to N
public class findDuplicateNumber {
  public static void main(String[] args) {
    int[] nums = {1,3,4,2,2};
    int duplicate = findDuplicate(nums);
    System.out.println(duplicate);
  } 

  static int findDuplicate(int[] nums) {
    int i=0;
    while(i<nums.length){
      // if the num[i] != i+1 means that the number will not be of its position
     if(nums[i] != i+1) {
        // correct checks the correct position of the num
        int correct = nums[i]-1;
        // if below both are equal means that there must the same number present so it would be the duplicate number
        // else swap it with correct number
        if(nums[i] != nums[correct]) {
          swap(nums, i, correct);
        }
        else return nums[i];
      } 
      // if we got the position correct then just move by 1
      else i++;
    }
    return -1;
  }

  static void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
  }
}
