import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
  public static void main(String[] args) {
    int[] nums = {2,7,11,15};
    int target = 9;
    int[] res = sum(nums, target);
    System.out.println(Arrays.toString(res));
  }

  static int[] sum(int[] nums, int target) {
    HashMap<Integer, Integer> map = new HashMap<>();
    int a = 0;
    int b = 0;
    
    for(int i=0; i<nums.length; i++) {
      int diff = target-nums[i];
      if(map.containsKey(diff)) {
        a = map.get(diff);
        b=i;
        break;
      }
      else map.put(nums[i], i);
    }

    return new int[]{a,b};
  }
}
