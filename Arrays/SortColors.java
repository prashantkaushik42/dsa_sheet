import java.util.*;

public class SortColors {
  public static void main(String[] args) {
    int[] nums = {2,0,2,1,1,0};
    sort(nums);
    System.out.println(Arrays.toString(nums));
  }

  static void sort(int[] nums) {
    int n = nums.length;
    int start = 0;
    int mid = 0;
    int end = n-1;
    // check the mid element 
    // if mid == 0 then swap it with start and move start and mid by 1
    // if mid ==1 then just move mid by 1
    // if mid ==2 then swap it with end and move end by -1
    while(mid <= end) {
      switch (nums[mid]) {
        case 0: {
          swap(nums, start, mid);
          start++;
          mid++;
          break;
        }
        case 1: mid++; break;
        case 2: {
          swap(nums, mid, end);
          end--;
          break;
        }
      }
    }

  }

  static void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
  }
}

