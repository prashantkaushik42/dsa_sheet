import java.util.*;

public class RotateMatrix {
  public static void main(String[] args) {
    int[][] arr =  {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    rotate(arr);
    System.out.println(Arrays.deepToString(arr));
  }

  static void rotate(int[][] matrix) {
    // firstly transpose the matrix and then reverse every row
    int rows = matrix.length;
    int cols = matrix[0].length;
    // TRANSPOSE THE MATRIX
    for(int i=0; i<rows; i++ ){
      for(int j=i; j<cols; j++) {
        swap(matrix, i, j);
      }
    }
    
    // REVERSE EVERY ROW
    for(int i=0; i<rows; i++) {
      for(int j=0; j<rows/2; j++) {
        int temp = matrix[i][j];
        matrix[i][j] = matrix[i][rows-j-1];
        matrix[i][rows-j-1] = temp;
      }
    }

  }

  static void swap(int[][] matrix, int i, int j) {
    int temp = matrix[i][j];
    matrix[i][j] = matrix[j][i];
    matrix[j][i] = temp;
  }
}
