package Recursion;

import java.util.ArrayList;
import java.util.List;

/**
 * PalindromePartition
 */
public class PalindromePartition {
  public static void main(String[] args) {
    String str = "aab";
    List<List<String>> res = partition(str);
    System.out.println(res);
  }

  static List<List<String>> partition(String str) {
    List<List<String>> res = new ArrayList<>();
    List<String> inner = new ArrayList<>();
    // helper(0, str, inner, res);
    helper2(str, "", inner, res);
    return res;
  }

  // this is more optimized code
  static void helper(int idx, String str, List<String> inner, List<List<String>> res) {
    if (idx == str.length()) {
      res.add(new ArrayList<>(inner));
      return;
    }

    for (int i = idx; i < str.length(); ++i) {
      if (isPalindrome(str, idx, i)) {
        inner.add(str.substring(idx, i + 1));
        helper(i + 1, str, inner, res);
        inner.remove(inner.size() - 1);
      }
    }
  }

  static boolean isPalindrome(String s, int start, int end) {
    while (start <= end) {
      if (s.charAt(start++) != s.charAt(end--))
        return false;
    }
    return true;
  }

  static void helper2(String str,String up, List<String> inner, List<List<String>> res) {
    if(str.length() == 0) {
      res.add(new ArrayList<>(inner));
      return;
    }

    for(int i=0; i<str.length(); i++) {
      String prefix = str.substring(0, i+1);
      String left = str.substring(i+1);
      if(isPalindrome(prefix)) {
        inner.add(prefix);
        helper2(left, up+prefix, inner, res);
        inner.remove(inner.size()-1);
      }
    }
  }

  static boolean isPalindrome(String str) {
    int l= 0;
    int r=str.length()-1;
    while (l<=r) {
      if(str.charAt(l++) != str.charAt(r--)) return false; 
    }
    return true;
  }
}
