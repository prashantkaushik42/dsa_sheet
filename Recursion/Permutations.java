package Recursion;

import java.util.ArrayList;
import java.util.List;

public class Permutations {
  public static void main(String[] args) {
    int[] nums = { 1,2,3 };
    List<List<Integer>> res = permute(nums);
    System.out.println(res);
  }

  static List<List<Integer>> permute(int[] nums) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> inner = new ArrayList<>();
    helper(nums, res, inner);
    return res;
  }

  static void helper(int[] nums, List<List<Integer>> res, List<Integer> inner) {
    if(inner.size() == nums.length) {
      res.add(new ArrayList<>(inner));
      return;
    }

    for (int i = 0; i < nums.length; i++) {
      if(!inner.contains(nums[i])) {
        inner.add(nums[i]);
        helper(nums, res, inner);
        inner.remove(inner.size() - 1);
      }
    }
  }

}
