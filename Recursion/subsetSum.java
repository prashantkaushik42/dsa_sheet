package Recursion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class subsetSum {
  public static void main(String[] args) {
    ArrayList<Integer> arr = List.of(5,2,1);
  }  

  static ArrayList<Integer> subset_sum(ArrayList<Integer> arr, int N) {
    ArrayList<Integer> res = new ArrayList<>();
    subsetHelper(0, 0, arr, arr.size(), res);
    Collections.sort(res);
    return res;
  }
  
  static void subsetHelper(int idx, int sum, ArrayList<Integer> arr, int N, ArrayList<Integer> res) {
    if(idx == N) {
      res.add(sum);
      return;
    }

    // decide to include the element
    subsetHelper(idx+1, sum + arr.get(idx), arr, N, res);
    // decide not to include the element
    subsetHelper(idx+1, sum, arr, N, res);
  }
}
