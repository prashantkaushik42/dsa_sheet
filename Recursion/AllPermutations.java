package Recursion;

import java.util.ArrayList;
import java.util.List;

/**
 * AllPermutations
 */
public class AllPermutations {
  public static void main(String[] args) {
    String str = "1234";
    List<String> res = getPermutaions(str);
    System.out.println(res);
  }

  static List<String> getPermutaions(String str) {
    List<String> res = new ArrayList<>();
    helper(str, "", res);
    return res;
  }

  static void helper(String str, String up, List<String> res) {
    if(str.length() == 0) {
      res.add(up);
      return;
    }

    for(int i=0; i<str.length(); i++) {
      char ch = str.charAt(i);
      String ros = str.substring(0,i) + str.substring(i+1);
      helper(ros, up+ch, res);
    }
  }
  
}
