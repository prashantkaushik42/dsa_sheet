package Recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * CombinationSum2
 */
public class CombinationSum2 {
  public static void main(String[] args) {
    int[] candidates = { 10,1,2,7,6,1,5 }; 
    int target = 8;
    // List<List<Integer>> res = combination2(candidates, target);
    List<List<Integer>> res = combination2OP(candidates, target);
    System.out.println(res);
  }

  static List<List<Integer>> combination2(int[] arr, int target) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> inner = new ArrayList<>();
    HashSet<List<Integer>> set = new HashSet<>();
    Arrays.sort(arr);
    dfs(0, 0, arr, target, res, inner, set);
    return res;
  }

  static void dfs(int idx, int sum, int[] arr, int target, List<List<Integer>> res, List<Integer> inner, HashSet<List<Integer>> set) {
    if(sum == target) {
      if(!set.contains(inner)){
        set.add(inner);
        res.add(new ArrayList<>(inner));
      }
      return;
    }

    if(idx >= arr.length || sum > target) return;

    inner.add(arr[idx]);
    dfs(idx+1, sum+arr[idx], arr, target, res, inner, set);
    inner.remove(inner.size()-1);
    dfs(idx+1, sum, arr, target, res, inner, set);
  }

    static List<List<Integer>> combination2OP(int[] arr, int target) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> inner = new ArrayList<>();
    Arrays.sort(arr);
    dfs(0, 0, arr, target, res, inner);
    return res;
  }

  static void dfs(int idx, int sum, int[] arr, int target, List<List<Integer>> res, List<Integer> inner) {
    if(sum == target) {
      res.add(new ArrayList<>(inner));
      return;
    }

    for(int i=idx; i<arr.length; i++) {
      if(i > idx && arr[i] == arr[i-1]) continue;
      if(sum + arr[i] > target) break;

      inner.add(arr[i]);
      dfs(i+1, sum+arr[i], arr, target, res, inner);
      inner.remove(inner.size()-1);
    }
  }

}
