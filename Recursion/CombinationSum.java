package Recursion;

import java.util.ArrayList;
import java.util.List;

/**
 * CombinationSum
 */
public class CombinationSum {
  public static void main(String[] args) {
    int[] candidates = { 2, 3, 6, 7 };
    int target = 7;
    List<List<Integer>> res = combinations(candidates, target);
    System.out.println(res);
  }

  static List<List<Integer>> combinations(int[] candidates, int target) {
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> inner = new ArrayList<>();
    dfs(0, 0, candidates, target, res, inner);
    return res;
  }

  static void dfs(int idx, int sum, int[] candidates, int target, List<List<Integer>> res, List<Integer> inner) {
    if (sum == target) {
      res.add(new ArrayList<>(inner));
      return;
    }

    if (idx >= candidates.length || sum > target) {
      return;
    }

    inner.add(candidates[idx]);
    dfs(idx,sum + candidates[idx], candidates, target, res, inner);
    inner.remove(inner.size() - 1);
    dfs(idx + 1,sum  ,candidates, target, res, inner);
  }
}
