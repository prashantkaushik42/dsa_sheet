package Recursion;

import java.util.ArrayList;

/**
 * RatInAMaze
 */
public class RatInAMaze {
  public static void main(String[] args) {
    int n = 4;
    int[][] m = { { 1, 0, 0, 0 },
        { 1, 1, 0, 1 },
        { 1, 1, 0, 0 },
        { 0, 1, 1, 1 } };
    ArrayList<String> res = findPath(m, n);
    System.out.println(res);
  }

  static ArrayList<String> findPath(int[][] m, int n) {
    ArrayList<String> res = new ArrayList<>();
    boolean[][] visited = new boolean[n][n];
    helper(0, 0, res, m, n, "", visited);
    return res;
  }

  static void helper(int row, int col, ArrayList<String> res, int[][] m, int n, String str, boolean[][] visited) {
    if (row == n - 1 && col == n - 1 && m[row][col] == 1) {
      res.add(str);
      return;
    }

    if (row >= n || row < 0)
      return;
    if (col >= n || col < 0)
      return;
    if(visited[row][col] || m[row][col] == 0) return;

    visited[row][col] = true;
    helper(row+1, col, res, m, n, str.concat("D"), visited);
    helper(row, col-1, res, m, n, str.concat("L"), visited);
    helper(row, col+1, res, m, n, str.concat("R"), visited);
    helper(row-1, col, res, m, n, str.concat("U"), visited);
    visited[row][col] = false;
  }
}
