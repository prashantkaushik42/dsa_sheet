package Recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Subset2
 */
public class Subset2 {
  public static void main(String[] args) {
    int[] nums = { 1, 2, 2 };
    // List<List<Integer>> res = subsets_using_set(nums);
    List<List<Integer>> res = subsetOP(nums); 
    System.out.println(res);
  }

  // here we are using hashset to check if it is already included into res or not
  static List<List<Integer>> subsets_using_set(int[] nums) {
    List<List<Integer>> res = new ArrayList<>();
    HashSet<List<Integer>> set = new HashSet<>();
    List<Integer> inner = new ArrayList<>();
    Arrays.sort(nums);
    subsetHelper(0, nums.length, nums, set, res, inner);
    return res;
  }

  static void subsetHelper(int idx, int N, int[] nums, HashSet<List<Integer>> set, List<List<Integer>> res,
      List<Integer> inner) {
    if (idx == N) {
      if(!set.contains(inner)) {
        set.add(new ArrayList<>(inner));
        res.add(new ArrayList<>(inner));
      }
      return;
    }

    inner.add(nums[idx]);
    subsetHelper(idx + 1, N, nums, set, res, inner);
    inner.remove(inner.size() - 1);
    subsetHelper(idx + 1, N, nums, set, res, inner);
  }

  // This is the optimized solution
  // if we skip the particular element then we need to skip all the element same to particular element
  static List<List<Integer>> subsetOP(int[] nums) {
    List<List<Integer>> res = new ArrayList<>();
    Arrays.sort(nums);
    dfs(0, nums, res, new ArrayList<>());
    return res;
  }

  static void dfs(int idx, int[] nums, List<List<Integer>> res, List<Integer> inner) {
    if(idx == nums.length) {
      res.add(new ArrayList<>(inner));
      return;
    }

    inner.add(nums[idx]);
    dfs(idx+1, nums, res, inner);
    inner.remove(inner.size()-1);
    while (idx +1 < nums.length && nums[idx] == nums[idx+1]) {
      idx += 1; 
    }
    dfs(idx+1, nums, res, inner);
  }
}
