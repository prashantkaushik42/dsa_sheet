package Recursion;

/**
 * Main
 */
public class Main {
 public static void main(String[] args) {
    // print(10);
    print_rev(10);
 } 

 static void print(int n) {
   if(n == 0) return; 
   print(n-1);
   System.out.println(n);
 }

 static void print_rev(int n) {
   if(n == 0) return;
   System.out.println(n);
   print_rev(n-1);
 }


 
}
