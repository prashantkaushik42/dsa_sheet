package Recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import java.util.LinkedList;

/**
 * NQueen
 */

//This is a brute-force solution where we check for every instance of the col and the diagonal at upper side and lower side with positive and negative diagonal
//For optimization we can be using the Hashsets for storing the values of the postition of the Queen at each position which makes it even optimized
//Here we can use three sets 
// 1 -> for cols 
// 2 -> for positive diagonal (r-c) is same throughout the diagonal 
// 3 -> for neegative diagonal (r+c) is same throughout the diagonal
public class NQueen {
  public static void main(String[] args) {
    int n = 4;
    List<List<String>> res = solveNQueens(n);
    System.out.println(res);
  }

  static List<List<String>> solveNQueens(int n) {
    char[][] board = new char[n][n];
    for (int i = 0; i < n; i++) {
      Arrays.fill(board[i], '.');
    }
    List<List<String>> res = new ArrayList<>();
    dfs(0, board, res);
    return res;
  }

  static void dfs(int col, char[][] board, List<List<String>> res) {
    if (col == board.length) {
      res.add(construct(board));
      return;
    }

    for (int row = 0; row < board.length; row++) {
      if (validate(board, row, col)) {
        board[row][col] = 'Q';
        dfs(col + 1, board, res);
        board[row][col] = '.';
      }
    }
  }

  static boolean validate(char[][] board, int row, int col) {
    int duprow = row;
    int dupcol = col;

    while (row >= 0 && col >= 0) {
      if (board[row][col] == 'Q')
        return false;
      row--;
      col--;
    }

    row = duprow;
    col = dupcol;

    while (col >= 0) {
      if (board[row][col] == 'Q')
        return false;
      col--;
    }

    row = duprow;
    col = dupcol;
    while (col >= 0 && row < board.length) {
      if (board[row][col] == 'Q')
        return false;
      row++;
      col--;
    }
    
    return true;
  }

  static List<String> construct(char[][] board) {
    List<String> res = new LinkedList<>();
    for (int i = 0; i < board.length; i++) {
      String s = new String(board[i]);
      res.add(s);
    }
    return res;
  }
}
