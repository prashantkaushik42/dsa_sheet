package problems;

import java.net.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 * IPO
 */
public class IPO {
  static class Project implements Comparable<Project>{
    int capital, profit;
    public Project(int capital, int profit) {
      this.capital = capital;
      this.profit = profit;
    }
  
    public int compareTo(Project project) {
      return capital - project.capital;
    }
  }

  public static void main(String[] args) {
    int k = 2, w = 0;
    int[] profits = { 1,2,3 }, capital = { 0,1,1 };
    int ans = findMaximizedCapital(k, w, profits, capital);
    System.out.println(ans);
  }

  static int findMaximizedCapital(int k, int w, int[] profits, int[] capital) {
    int n = profits.length;
    Project[] projects = new Project[n];
    for(int i=0; i<n; i++) {
      projects[i] = new Project(capital[i], profits[i]);
    }
    Arrays.sort(projects);
    PriorityQueue<Integer> pq = new PriorityQueue<>(n, Collections.reverseOrder());
    int ptr = 0;
    for(int i=0; i<k; i++) {
      while(ptr < n && projects[ptr].capital <= w) {
        pq.add(projects[ptr++].profit);
      }
      if(pq.isEmpty()) {
        break;
      }
      w += pq.poll();
    }
    return w;
  }
}
