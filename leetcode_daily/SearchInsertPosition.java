package problems;

/**
 * SearchInsertPosition
 */
public class SearchInsertPosition {
  public static void main(String[] args) {
    int[] nums = {1,3,5,6};
    int target = 2;

    int idx = search(nums, target);
    System.out.println(idx);
  }

  static int search(int[] nums, int target) {
    int l = 0;
    int r = nums.length - 1;
    while(l<=r) {
      if(target < nums[l]) return l;
      else if(target > nums[r]) return r+1;
      int mid = l + (r-l)/2;
      if(nums[mid] == target) return mid;
      else if(target > nums[mid]) l=mid+1;
      else r = mid-1;
    }
    return 0;
  }
}
