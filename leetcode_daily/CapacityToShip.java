package problems;

import java.util.Arrays;

public class CapacityToShip {
  public static void main(String[] args) {
    int[] weights = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int days = 5;
    int ans = shipWithinDays(weights, days);
    System.out.println(ans);
  }

  static int shipWithinDays(int[] weights, int days) {
    int l = Arrays.stream(weights).max().getAsInt();
    int r = Arrays.stream(weights).sum();
    int res = r;

    while (l <= r) {
      int mid = l + (r - l) / 2;
      if (canShip(mid, weights, days)) {
        res = Math.min(res, mid);
        r = mid - 1;
      } else
        l = mid + 1;
    }
    return res;
  }

  static boolean canShip(int cap, int[] weights, int days) {
    int ships = 1, currCap = cap;
    for (int w : weights) {
      if (currCap - w < 0) {
        ships += 1;
        currCap = cap;
      }
      currCap -= w;
    }
    return ships <= days;
  }
}
