package leetcode_daily;

import java.util.Collections;
import java.util.PriorityQueue;

public class MinimizeDeviation {
  public static void main(String[] args) {
    int[] nums = {1,2,3,4};
    int ans = minimumDev(nums);
    System.out.println(ans);
  }

  static int minimumDev(int[] nums) {
    PriorityQueue<Integer> maxh = new PriorityQueue<>(Collections.reverseOrder());
    int m = Integer.MAX_VALUE;
    int diff = Integer.MAX_VALUE;
    for(int i: nums){
      if(i%2!=0) i *= 2;
      m = Math.min(m, i);
      maxh.add(i);
    }

    while(maxh.peek()%2 == 0){
      int mx = maxh.peek();
      maxh.poll();
      diff = Math.min(diff, mx-m);
      m = Math.min(m, mx/2);
      maxh.add(mx/2);
    }

    return Math.min(diff, maxh.poll() - m);
  }
}
