package leetcode_daily;

/**
 * NoOfZeroFilledSubArray
 */
public class NoOfZeroFilledSubArray {
  public static void main(String[] args) {
   int[] nums = { 1,3,0,0,2,0,0,4 };
   long ans = zeroFilled(nums);
   System.out.println(ans);
  } 

  static long zeroFilled(int[] nums) {
    long count = 0;
    long curr_count = 0;

    for(int i=0; i<nums.length; i++) {
      if(nums[i] == 0) {
        count += curr_count + 1;
        curr_count++;
      }
      else{
        curr_count = 0;
      }
    }
    return count;
  }
}
