package leetcode_daily;

import java.util.HashSet;
import java.util.Set;

public class KthMissing {
  public static void main(String[] args) {
    int[] arr = { 2, 3, 4, 7, 11 };
    int k = 5;
    int res = findkthPositive(arr, k);
    System.out.println(res);
  }

  static int findkthPositive(int[] arr, int k) {
    Set<Integer> set = new HashSet<>();
    for (int el : arr) {
      set.add(el);
    }

    int count = 0;
    int i = 1;
    while (count < k) {
      if (!set.contains(i)) {
        count++;
      }
      if (count < k)
        i++;
    }

    return i;
  }
}
