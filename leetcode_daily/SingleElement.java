package problems;

/**
 * SingleElement
 */
public class SingleElement {
  public static void main(String[] args) {
    int[] nums = { 1, 1, 2, 3, 3, 4, 4, 8, 8 };
    // int ans = singleNonDuplicate(nums);
    int ans = singleNonDuplicateOP(nums);
    System.out.println(ans);
  }

  // XOR of two same number will be zero;
  // and XOR of 0 and num == num
  static int singleNonDuplicate(int[] nums) {
    int ans = 0;
    for (int i = 0; i < nums.length; i++) {
      ans = ans ^ nums[i];
    }

    return ans;
  }

  // if the mid is even then next element is should also be same
  // else mid is odd then prev element should also be same
  static int singleNonDuplicateOP(int[] nums) {
    int start = 0;
    int end = nums.length - 1;
    while (start < end) {
      int mid = (start + end) / 2;
      if ((mid % 2 == 0 && nums[mid] == nums[mid + 1]) || (mid % 2 == 1 && nums[mid] == nums[mid - 1])) {
        start = mid + 1;
      } else
        end = mid;
    }
    return nums[start];
  }
}
